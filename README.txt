S'ha dissenyat un conjunt d'estructures de dades per a controlar l'estoc d'una
tenda esportiva. En aquesta, es poden realitzar les següents operacions:
- Preparar l'estructura buida per emmagatzemar els productes.
- Introduir un producte amb una marca, un codi, un nom i un nombre d'unitats.
- Esborrar el producte amb el codi donat.
- Imprimir tots els productes d'una marca.
- Imprimir tots els productes de la tenda. Ordenats ascendentment pel seu codi.

A més, conté un programa per simular el funcionament i també permet provar-ho.