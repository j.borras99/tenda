-- Batomeu Ramis Tarrag�
-- Josep Borr�s S�nchez

with Ada.Text_IO;
use Ada.Text_IO;

with Ada.Strings.Unbounded;
use Ada. Strings. Unbounded;

with Ada.Strings.Unbounded.Text_IO;

with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;

with d_estoc;
use d_estoc;

procedure Tenda is

   stc: estoc;

   rpt: Boolean := True;
   opt: Integer;


   k: Integer;
   nom: Unbounded_String;
   cant: Integer;
   mrc: Integer;

begin
   estoc_buit(stc);

   while rpt loop
      Put_Line("********* ESTOC *********");
      Put_Line("Estoc de la tenda. Que voleu fer?");
      Put_Line("[1] - Posar producte.");
      Put_Line("[2] - Esborrar producte.");
      Put_Line("[3] - Productes d'una marca.");
      Put_Line("[4] - Productes de l'estoc.");
      Put_Line("[5] - Buidar l'estoc.");
      Put_Line("[0] - Sortir.");
      Put_Line("            Introdueix una opcio:");
      Get(opt);
      Skip_Line;
      New_Line;
      case opt is
         when 0 =>
            Put_Line("GRACIES !!");
            rpt := False;
         when 1 => --inserim un producte dins l'estoc
            Put_Line("Insertar un producte.");

            for i in marca loop
               Put(i'Enum_Rep'Image&"-"&i'Image&" | ");
            end loop;
            New_Line;
            Put("Marca: ");
            Get(mrc);
            Skip_Line;
            Put("Nom: ");
            Ada.Strings.Unbounded.Text_IO.Get_Line(nom);
            Put("Codi: ");
            Get(k);
            Skip_Line;
            Put("Quantitat: ");
            Get(cant);
            Skip_Line;
            posar_producte(stc, marca'Val(mrc) ,codigo(k), nombre(nom), cant);
            New_Line;
            Put_Line("FET !!");

         when 2 => --Eliminar un producto de dins l'estoc
            Put_Line("Eliminar un producte");

            Put("Codi: ");
            Get(k);
            Skip_Line;
            esborrar_producte(stc, codigo(k));

            Put_Line("FET !!");

         when 3 => -- Imprimeix tots els productes d'una marca
            Put_Line("Imprimir estoc d'una marca");

            for i in marca loop
               Put(i'Enum_Rep'Image&"-"&i'Image&" | ");
            end loop;
            New_Line;
            Put("Marca: ");
            Get(mrc);
            Skip_Line;

            imprimir_productes_marca(stc, marca'Val(mrc));
            Put_Line("FET !!");

         when 4 => -- Imprimeix tots els productes de dins l'estoc
            Put_Line("Imprimir estoc de la tenda");

            imprimir_estoc_total(stc);
            Put_Line("FET !!");

         when 5 => -- Feim un estoc_buid i ens buidar� tot l'estoc de la tenda.
            estoc_buit(stc);
            Put_Line("FET !!");
         when others => --En cas d'esntrar una opcio no valida
            Put_Line("Opcio no valida");
      end case;
      New_Line;
      New_Line;
      New_Line;
   end loop;

end Tenda;
