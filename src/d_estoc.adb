with Ada.Text_IO;
use Ada.Text_IO;

package body d_estoc is

   --Funcions necessaries per a poder comparar codis en els arbres.
   function "<" (k1, k2: in codigo) return boolean is
   begin
      return Integer(k1) < Integer(k2);
   end "<";

      function ">" (k1, k2: in codigo) return boolean is
   begin
      return Integer(k1) > Integer(k2);
   end ">";

   procedure estoc_buit(c: out estoc) is
      mrs: marques renames c.m;
      set: conjunto renames c.set_p;

   begin

      cvacio(set);

      for i in mrs'First..mrs'Last loop
         mrs(i) := null;
      end loop;

   end estoc_buit;

   procedure posar_producte(c: in out estoc; m: in marca; k: in codigo; n: in nombre; unitats: in Integer) is
      mrs: marques renames c.m;
      set: conjunto renames c.set_p;
      pp: pproduct;
   begin
      pp := new producto;
      pp.nom := n;
      pp.cod := k;
      pp.unit := unitats;
      pp.mrc := m;

      --arbre
      poner(set,k,pp);

      --llista
      if mrs(m) = null then
         mrs(m) := pp;
         pp.ant := null;
         pp.sig := null;
      else
         mrs(m).ant := pp;
         pp.sig := mrs(m);
         mrs(m) := pp;
         pp.ant := null;
      end if;

   exception

      when tree_product.ya_existe => raise d_estoc.ya_existe;
      when tree_product.espacio_desbordado => raise d_estoc.espai_desbordat;
   end posar_producte;

   procedure esborrar_producte(c: in out estoc; k: in codigo) is
      mrs: marques renames c.m;
      set: conjunto renames c.set_p;
      pp: pproduct;
      mrc: marca;
   begin
      --arbre
      consultar(set, k, pp);
      borrar(set, k);

      --llista
      if pp.ant = null and pp.sig = null then --primer de la llista
         mrc := pp.mrc;
         mrs(mrc) := null;
      elsif pp.sig /=null and pp.ant = null then
         mrc := pp.mrc;
         mrs(mrc) := pp.sig;
         pp.sig.ant := null;
         --
         pp.sig := null;
         --
      elsif pp.sig = null and pp.ant /= null then
         pp.ant.sig := null;
         --
         pp.ant := null;
         --
      else
         pp.ant.sig := pp.sig;
         pp.sig.ant := pp.ant;
      end if;

   exception

         when tree_product.no_existe => raise d_estoc.no_exist;
   end esborrar_producte;

   procedure imprimir_productes_marca(c: in estoc; m: in marca) is
      mrs: marques renames c.m;
      pp: pproduct;
   begin
      pp := mrs(m);
      if pp /= null then
         while pp /= null loop
            Put_Line(pp.cod'Image&"   "&To_String(pp.nom)&"      "&pp.unit'Image);
            pp := pp.sig;
         end loop;
      end if;
   end imprimir_productes_marca;

   -- INORDRE
   procedure imprimir_estoc_total(c: in estoc) is
      set: conjunto renames c.set_p;
      it: iterador;
      pp: pproduct;
      k: codigo;
   begin

      first(set, it);

      while is_valid(it) loop
         pp := new producto;
         get(set, it, k, pp);
         Put_Line(pp.cod'Image&"   "&To_String(pp.nom)&"      "&pp.unit'Image);
         next(set, it);
      end loop;

   exception

         when tree_product.bad_use => raise d_estoc.mal_us;
   end imprimir_estoc_total;

end d_estoc;
