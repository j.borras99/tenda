-- ** PILA ENLA�ADA AMB PUNTERS **
-- la utilitzarem per a l'iterador de l'arbre

generic
   -- es crea l'elemento de car�cter gen�ric.
   type elem is private;

package dpila is
   -- variables de la pila.
   type pila is limited private;

   mal_uso: exception;
   espacio_desbordado: exception;

   -- instanciaci� dels procedimients/operacions propies d'una pila.
   procedure pvacia(p: out pila);
   procedure empila(p: in out pila; x: in elem);
   procedure desempila(p: in out pila);
   function estavacia(p: in pila)return boolean;
   function cima(p: in pila) return elem;

private
  -- descripci� de la pila. Part transparent per l'usuario.
   type nodo;
   type pnodo is access nodo;
   type nodo is record
      x: elem;
      seg: pnodo;
   end record;
   type pila is record
      top: pnodo;
   end record;
end dpila;
