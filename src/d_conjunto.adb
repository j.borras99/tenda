package body d_conjunto is
   type modo is (insert_mode, remove_mode);

-- Operacions del CONJUNT.
   procedure cvacio (s : out conjunto) is
      raiz: pnodo renames s.raiz;
   begin
      raiz := null;
   end cvacio;


   procedure rebalanceo_izq (p : in out pnodo; h : out boolean; m : in modo) is

      a : pnodo; -- el nodo inicialmente en la raiz
      b : pnodo; -- hijo izq de a
      c, b2 : pnodo; -- hijo der de b
      c1, c2 : pnodo; -- hijos izq y der de c
   begin
      a := p; b := a.lc;
      if b.bl <= 0 then -- promociona b
	 b2 := b.rc; -- asigna nombre
	 a.lc := b2;
	 b.rc := a;
	 p := b; -- reestructura
	 if b.bl = 0 then
	    -- actualiza bl y h
	    a.bl := -1;
	    b.bl := 1;
	    if m = remove_mode then
	       h := false;
	    end if ; -- else h se mantiene a true
	 else
	    -- b.bl= -1
	    a.bl := 0;
	    b.bl := 0;
	    if m = insert_mode then
	       h := false;
	    end if ;-- else h se mantiene a true
	 end if ;
      else -- promociona c
	 c := b.rc;
	 c1 := c.lc;
	 c2 := c.rc; -- asigna nombres
	 b.rc := c1;
	 a.lc := c2;
	 c.lc := b;
	 c.rc := a;
	 p := c; -- reestructura
	 if c.bl <= 0 then
	    b.bl := 0;
	 else
	    b.bl := -1;
	 end if ;
	 -- actualiza bl y h
	 if c.bl >= 0 then
	    a.bl := 0;
	 else
	    a.bl := 1;
	 end if ;
	 c.bl := 0;
	 if m = insert_mode then
	    h := false;
	 end if ; --  else h se mantiene a true
      end if ;
   end rebalanceo_izq;

   procedure balanceo_izq (p : in out pnodo; h : in out boolean; m : in modo) is

      -- o p.rc ha decrecido un nivel (por borrado)
   begin
      if p.bl = 1 then
	 p.bl := 0;
	 if m = insert_mode then
	    h := false;
	 end if ;
	 -- else h se mantiene a true
      elsif p.bl = 0 then

	 p.bl := -1;
	 if m = remove_mode then
	    h := false;
	 end if ; -- else h se mantiene a true
      else -- p.bl=-1
	 rebalanceo_izq (p, h, m);
      end if ;
   end balanceo_izq;


   procedure rebalanceo_der (p : in out pnodo; h : in out Boolean; m : in modo) is
      a : pnodo;
      b : pnodo;
      c, b2 : pnodo;
      c1, c2 : pnodo;
   begin
      a := p;
      b := a.rc;
      if b.bl >= 0 then
	 b2 := b.lc;
	 a.rc := b2;
	 b.lc := a;
	 p := b;
	 if b.bl = 0 then
	    a.bl := 1;
	    b.bl := -1;
	    if m = remove_mode then h := False; end if;
	 else
	    a.bl := 0;
	    b.bl := 0;
	    if m = insert_mode then h := False; end if;
	 end if;
      else
	 c := b.lc;
	 c1 := c.rc;
	 c2 := c.lc;
	 b.lc := c1;
	 a.rc := c2;
	 c.rc := b;
	 c.lc := a;
	 p := c;
	 if c.bl <= 0 then b.bl := 0;
	 else
	    b.bl := 1;
	 end if;
	 if c.bl >= 0 then a.bl := 0;
	 else
	    a.bl := -1;
	 end if;
	 if m = insert_mode then h := False; end if;
      end if;
   end rebalanceo_der;

   procedure balanceo_der (p : in out pnodo; h : in out Boolean; m : in modo) is
   begin
      if p.bl = -1 then
	 p.bl := 0;
	 if m = insert_mode then
	    h := False;
	 end if;
      elsif p.bl = 0 then
	 p.bl := 1;
	 if m = remove_mode then
	    h := false;
	 end if;
      else
	 rebalanceo_der (p, h, m);
      end if;
   end balanceo_der;


   procedure poner (p : in out pnodo; k : in key; x : in item; h : out Boolean) is
   begin
      if p = null then
         p := new nodo;
         p.k := k;
         p.x := x;
         p.bl := 0;
         p.lc := null;
         p.rc := null;
	 h := true; else
	 if k < p.k then
            poner(p.lc, k, x, h);
	    if h then balanceo_izq (p, h, insert_mode);
	    end if;
	 elsif k > p.k then
            poner(p.rc, k, x, h);
	    if h then
	       balanceo_der (p, h, insert_mode);
	    end if ;
	 else -- k=p.k
	    raise ya_existe;
	 end if ;
      end if ;
   exception
      when storage_error => raise espacio_desbordado;
   end poner;

   procedure poner (s : in out conjunto; k : in key; x : in item) is
      raiz : pnodo renames s.raiz;
      h: Boolean;
   begin
      poner(raiz, k, x, h);
   end poner;


   procedure consultar (s : in pnodo; k : in key; x : out item) is
   begin
      if s = null then
	 raise no_existe;
      else
	 if k < s.k then consultar (s.lc, k, x);
	 elsif k > s.k then consultar (s.rc, k, x);
	 else x := s.x;
	 end if;
      end if;
   end consultar;

   procedure consultar (s : in conjunto; k : in key; x : out item) is
      raiz: pnodo renames s.raiz;
   begin
      consultar(raiz, k, x);
      null;
   end consultar;


   procedure borrado_masbajo (p : in out pnodo; pmasbajo : out pnodo; h : out boolean) is
      -- Prec.: p/=null
   begin      if p.lc /= null then
	 borrado_masbajo (p.lc, pmasbajo, h);
	 if h then balanceo_der (p, h, remove_mode);
	 end if ;
      else
	 pmasbajo := p;
	 p := p.rc;
	 h := true;
      end if ;
   end borrado_masbajo;

   procedure borrado_real (p : in out pnodo; h : out boolean) is
      -- Prec.: p.k = k
      pmasbajo : pnodo;
   begin
      if p.lc = null and p.rc = null then
	 p := null;
	 h := true;
      elsif p.lc = null and p.rc /= null then
	 p := p.rc;
	 h := true;
      elsif p.lc /= null and p.rc = null then
	 p := p.lc;
	 h := true;
      else -- s.lc/=null and s.rc/=null
	 borrado_masbajo (p.rc, pmasbajo, h);
	 pmasbajo.lc := p.lc;
	 pmasbajo.rc := p.rc;
	 pmasbajo.bl := p.bl;
	 p := pmasbajo;
	 if h then
	    balanceo_izq (p, h, remove_mode);
	 end if ;
      end if ;
   end borrado_real;

   procedure borrar (p : in out pnodo; k : in key; h: out Boolean) is
   begin
      if p = null then raise no_existe; end if;
      if k < p.k then
	 borrar (p.lc, k, h);
	 if h then
	    balanceo_der (p, h, remove_mode);
	 end if;
      elsif k > p.k then
	 borrar (p.rc, k, h);
	 if h then
	    balanceo_izq (p, h, remove_mode);
	 end if;
      else
	 borrado_real (p, h);
      end if;
   end borrar;

    procedure borrar (s : in out conjunto; k : in key) is
      raiz : pnodo renames s.raiz;
      h: Boolean;
   begin
      borrar (raiz, k, h);
   end borrar;


   procedure actualitza (s : in out pnodo; k : in key; x : in item) is
   begin
      if s = null then
	 raise no_existe;
      else
	 if k < s.k then
	    actualitza (s.lc, k, x);
	 elsif k > s.k then
	    actualitza (s.rc, k, x);
	 else
	    s.x := x;
	 end if ;
      end if ;
   end actualitza;

   procedure actualitza (s : in out conjunto; k : in key; x : in item) is
      raiz : pnodo renames s.raiz;

   begin
      actualitza (raiz, k, x);
   end actualitza;


-- Operaciones de l'ITERADOR.
   procedure first(s: in conjunto; it: out iterador) is
      root: pnodo renames s.raiz;
      st: pila renames it.st;
      p: pnodo;
   begin
      pvacia(st);  -- vaciamos la pila
      if root /= null then
         p:= root;
         while p.lc /= null loop  --buscamos hijo m�s a la izquierda
            empila(st, p);
            p:=p.lc;
         end loop;
         empila(st, p);
      end if;
   end first;

   procedure next(s: in conjunto; it: in out iterador) is
      st: pila renames it.st;
      p: pnodo;
   begin
      p:=cima(st);
      desempila(st);
      if p.rc /= null then -- si el nodo actual tiene hijo derecho
         p:=p.rc;
         while p.lc /= null loop
            empila(st, p);
            p:=p.lc;  -- nodo actual, el nodo izquierdo
         end loop;
         empila(st, p);
      end if;
   exception
      when dnodestack.mal_uso => raise d_conjunto.bad_use;
   end next;

   procedure get(s: in conjunto; it: in iterador; k: out key; x: out item) is
      st: pila renames it.st;
      p:pnodo;
   begin
      p:=cima(st);
      k:=p.k;
      x:=p.x;
   end get;

   function is_valid(it: in iterador) return boolean is
      st: pila renames it.st;
   begin
      return not estavacia(st);
   end is_valid;

end d_conjunto;
