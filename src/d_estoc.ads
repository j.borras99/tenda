-- Especificaciˇ del TAD estoc
with d_conjunto;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

package d_estoc is
   type estoc is limited private;
   type marca is (Nike, Adidas, Reebok, Asics, Fila, Puma, Quiksilver, Kappa, Joma, Converse);
   type codigo is new Integer range 0..5000000;
   type nombre is new Unbounded_String;

   mal_us: exception;
   espai_desbordat: exception;
   no_exist: exception;
   ya_existe: exception;

   procedure estoc_buit(c: out estoc);
   procedure posar_producte(c: in out estoc; m: in marca; k: in codigo; n: in nombre; unitats: in Integer);
   procedure esborrar_producte(c: in out estoc; k: in codigo);
   procedure imprimir_productes_marca(c: in estoc; m: in marca);
   procedure imprimir_estoc_total(c: in estoc);

private
   -- Tipus producte.
   type producto;
   type pproduct is access producto;
   type producto is record
      nom: nombre;
      cod: codigo;
      unit: Integer;
      mrc: marca;
      sig: pproduct;
      ant: pproduct;
   end record;

   -- Crearem un array indexat per les marques.
   type marques is array (marca) of pproduct;

   function "<" (k1, k2: in codigo) return boolean;
   function ">" (k1, k2: in codigo) return boolean;

   -- Crearem l'arbre que contindrÓ el conjunt de productes que dins l'estoc.
   package tree_product is new d_conjunto(codigo, pproduct, "<", ">");
   use tree_product;

   type estoc is record
      m: marques;
      set_p: conjunto;
   end record;

end d_estoc;

