package body dpila is
   -- Procedmient que buida la pila per complet
   procedure pvacia(p: out pila) is
   begin
      p.top := null;
   end pvacia;

   -- Procediment que emipla l'element donat
   procedure empila(p: in out pila; x: in elem) is
      r: pnodo;
   begin
      r := new nodo'(x, p.top);
      p.top := r;
   exception
         when Storage_Error => raise espacio_desbordado;
   end empila;

   -- Procediment que desempilar� un element de la llista
   procedure desempila(p: in out pila) is
   begin
      p.top := p.top.seg;
   exception
         when Constraint_Error => raise mal_uso;
   end desempila;

   -- Funci� que ens mostrar� el primer element de la pila
   function cima(p: in pila) return elem is
   begin
      return p.top.x;
   exception
         when Constraint_Error => raise mal_uso;
   end cima;

   -- Funci� que ens dir� si la pila �s buida o no

   function estavacia(p: in pila) return boolean is
   begin
      return p.top=null;
   end estavacia;


end dpila;
