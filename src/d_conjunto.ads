-- ** CONJUNT **
-- implementat amb un arbre de cerca binaria AVL.
with dpila;

generic
   type key is private;
   type item is private;
   with function "<" (k1, k2: in key) return boolean;
   with function ">" (k1, k2: in key) return boolean;

package d_conjunto is
   -- variables del conjunt.
   type conjunto is limited private;
   -- variable iterador.
   type iterador is limited private;
   -- excepcions del conjunt.
   ya_existe: exception;
   no_existe: exception;
   espacio_desbordado: exception;
   --excepció de l'iterador.
   bad_use: exception;

    -- instanciació dels procedimients/operacions propies d'una conjunt.
   procedure cvacio(s: out conjunto);
   procedure poner (s : in out conjunto; k : in key; x : in item);
   procedure consultar (s : in conjunto; k : in key; x : out item);
   procedure borrar (s : in out conjunto; k : in key);
   procedure actualitza(s: in out conjunto; k: in key; x: in item);

   -- instanciació dels procediments/operacions propies de l'iterador.
   procedure first(s: in conjunto; it: out iterador);
   procedure next(s: in conjunto; it: in out iterador);
   procedure get(s: in conjunto; it: in iterador; k: out key; x: out item);
   function is_valid(it: in iterador) return boolean;

private
   -- part transparetn per a l'usuari. Propi del conjunt.
   type nodo;
   type pnodo is access nodo;
   type factor_balanceo is new Integer range -1..1;
   type nodo is record
      k: key;
      x: item;
      bl: factor_balanceo;
      lc, rc: pnodo;
   end record;
   type conjunto is record
      raiz: pnodo;
   end record;

   -- part transparetn per a l'usuari. Propi de l'iterador.
   package dnodestack is new dpila(pnodo);
   use dnodestack;

   type iterador is record
      st: pila;
   end record;
end d_conjunto;
